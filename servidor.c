#include <stdio.h> 
#include <netdb.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h> 
#define MAX 80 
#define PORT 3550
#define SA struct sockaddr 

char tablero[3][3] = { {' ', ' ', ' '},
                         {' ', ' ', ' '},
                         {' ', ' ', ' '} };
  
// Function designed for chat between client and server. 
void func(int sockfd) 
{ 
    char buff[MAX]; 
    int n,fila,columna,exito; 
    // infinite loop for chat 
    for (;;) { 
	do{
		bzero(buff, MAX); 
		//Dibuja el tablero
		dibujar_tablero();
		printf("> Ingrese fila,columna: \n");
		n=0;
		// copy server message in the buffer 
		exito=0;
        	while ((buff[n++] = getchar()) != '\n') 
            	; 
	
		//extracción de fila y columna
		fila= (int) buff[0] - 48;
		columna= (int) buff[2] - 48;
		if(fila <= 3 && fila>0 && columna <= 3 && columna >0){
			if(tablero[fila-1][columna-1] == ' '){
				tablero[fila-1][columna-1]= 'X';
				exito=1;
			}else{
				printf("\n Ingrese su jugada en un lugar vacío\n ");
			}
		}else{
			// if msg contains "Exit" then server exit and chat ended. 
			if (strncmp("exit", buff, 4) == 0) { 
			    printf("Server Exit...\n"); 
			    break; 
			}else{
			printf("\nIngrese un valor válido\n");
			}
		}
	}while(exito == 0);

	//Dibuja el tablero
	dibujar_tablero();
	
	//Chequea si alguien gana
	if(chequea_tablero(fila, columna) == 1){
		printf("\nGANASTE EL JUEGO!!!\n");
		break;
	}

        // and send that buffer to client 
        write(sockfd, buff, sizeof(buff)); 
  
        // read the message from client and copy it in buffer 
        read(sockfd, buff, sizeof(buff)); 

	// if msg contains "Exit" then server exit and chat ended. 
        if (strncmp("exit", buff, 4) == 0) { 
            printf("Server Exit...\n"); 
            break; 
        } 

        // print buffer which contains the client contents 
        printf("Jugada del cliente: %s ", buff); 
	//extracción de fila y columna
	fila= (int) buff[0] - 48;
	columna= (int) buff[2] - 48;
	tablero[fila-1][columna-1]= 'O';
        bzero(buff, MAX); 
        n = 0;  
    } 
} 

void dibujar_tablero() //char tablero[][3]
{
    printf(" %c | %c | %c \n", tablero[0][0], tablero[0][1], tablero[0][2]);
    printf("-----------\n");
    printf(" %c | %c | %c \n", tablero[1][0], tablero[1][1], tablero[1][2]);
    printf("-----------\n");
    printf(" %c | %c | %c \n", tablero[2][0], tablero[2][1], tablero[2][2]);
}

//Chequea si alguien gana
int chequea_tablero(int row, int col){
    if (tablero[row][0] == tablero[row][1] && tablero[row][1] == tablero[row][2] && tablero[row][1]!=' ') { /* Existe fila ganadora. */
        return 1;
    }else if (tablero[0][col] == tablero[1][col] && tablero[1][col] == tablero[2][col] && tablero[1][col]!=' ') { /* Existe columna ganadora. */        
        return 1;
    }else if ((row+col)%2 == 0) { /*Si forma parte de una diagonal*/
        /* Chequea diagonal principal */
        if (((row==0 && col==0) || (row==1 && col==1) || (row==2 && col==2)) && (tablero[1][1] == tablero[0][0] && tablero[1][1] == tablero[2][2]) && tablero[1][1] != ' ') {
            return 1;
        }
        /* Chequea diagonal secundaria. */
        if ( ((row==0 && col==2) || (row==1 && col==1) || (row==2 && col==0)) && (tablero[1][1] == tablero[0][2] && tablero[1][1] == tablero[2][0]) && tablero[1][1] != ' ') { 
            return 1;
        }
    }
    /* No winner, yet. */
    return 0;
}
  
// Driver function 
int main() 
{ 
    int sockfd, connfd, len; 
    struct sockaddr_in servaddr, cli; 
  
    // socket create and verification 
    sockfd = socket(AF_INET, SOCK_STREAM, 0); 
    if (sockfd == -1) { 
        printf("socket creation failed...\n"); 
        exit(0); 
    } 
    else
        printf("Socket successfully created..\n"); 
    bzero(&servaddr, sizeof(servaddr)); 
  
    // assign IP, PORT 
    servaddr.sin_family = AF_INET; 
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY); 
    servaddr.sin_port = htons(PORT); 
  
    // Binding newly created socket to given IP and verification 
    if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0) { 
        printf("socket bind failed...\n"); 
        exit(0); 
    } 
    else
        printf("Socket successfully binded..\n"); 
  
    // Now server is ready to listen and verification 
    if ((listen(sockfd, 5)) != 0) { 
        printf("Listen failed...\n"); 
        exit(0); 
    } 
    else
        printf("Server listening..\n"); 
    len = sizeof(cli); 
  
    // Accept the data packet from client and verification 
    connfd = accept(sockfd, (SA*)&cli, &len); 
    if (connfd < 0) { 
        printf("server acccept failed...\n"); 
        exit(0); 
    } 
    else
        printf("server acccept the client...\n"); 
  
    // Function for chatting between client and server 
    func(connfd); 
  
    // After chatting close the socket 
    close(sockfd); 
} 
