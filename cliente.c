#include <netdb.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#define MAX 80 
#define PORT 3550 
#define SA struct sockaddr 

char tablero[3][3] = { {' ', ' ', ' '},
                         {' ', ' ', ' '},
                         {' ', ' ', ' '} };

void func(int sockfd) 
{ 
    char buff[MAX]; 
    int n,fila,columna,exito; 
    for (;;) { 

	//lee lo que escribe el servidor
        bzero(buff, sizeof(buff)); 
        read(sockfd, buff, sizeof(buff)); 
        printf("Jugada del servidor : %s", buff); 
	//extracción de fila y columna
	fila= (int) buff[0] - 48;
	columna= (int) buff[2] - 48;
	tablero[fila-1][columna-1]= 'X';	
	//Dibuja el tablero
	dibujar_tablero();

	//escribe cliente
	do{
		bzero(buff, sizeof(buff)); 
		printf("> Inserte fila,columna: "); 
		n = 0; 
		while ((buff[n++] = getchar()) != '\n') 
		    ; 
		exito = 0;
		//extracción de fila y columna
		fila= (int) buff[0] - 48;
		columna= (int) buff[2] - 48;
		if(fila <= 3 && fila>0 && columna <= 3 && columna >0){
			if(tablero[fila-1][columna-1] == ' '){
				tablero[fila-1][columna-1]= 'O';
				exito=1;
			}else{
				printf("\n Ingrese su jugada en un lugar vacío\n ");
			}
		}else{
			if ((strncmp(buff, "exit", 4)) == 0) { 
			    printf("Client Exit...\n"); 
			    break; 
			}else{
				printf("\nIngrese un valor válido\n");
			}
		}
	}while(exito == 0);
	//Dibuja el tablero
	dibujar_tablero();

	//Chequea si alguien gana
	if(chequea_tablero(fila, columna) == 1){
		printf("\nGANASTE EL JUEGO!!!\n");
		break;
	}

        write(sockfd, buff, sizeof(buff)); 
	
        if ((strncmp(buff, "exit", 4)) == 0) { 
            printf("Client Exit...\n"); 
            break; 
        } 
    } 
} 


void dibujar_tablero() //char tablero[][3]
{
    printf(" %c | %c | %c \n", tablero[0][0], tablero[0][1], tablero[0][2]);
    printf("-----------\n");
    printf(" %c | %c | %c \n", tablero[1][0], tablero[1][1], tablero[1][2]);
    printf("-----------\n");
    printf(" %c | %c | %c \n", tablero[2][0], tablero[2][1], tablero[2][2]);
}

//Chequea si alguien gana
int chequea_tablero(int row, int col){
    if (tablero[row][0] == tablero[row][1] && tablero[row][1] == tablero[row][2] && tablero[row][1]!=' ') { /* Existe fila ganadora. */
        return 1;
    }else if (tablero[0][col] == tablero[1][col] && tablero[1][col] == tablero[2][col] && tablero[1][col]!=' ') { /* Existe columna ganadora. */        
        return 1;
    }else if ((row+col)%2 == 0) { /*Si forma parte de una diagonal*/
        /* Chequea diagonal principal */
        if (((row==0 && col==0) || (row==1 && col==1) || (row==2 && col==2)) && (tablero[1][1] == tablero[0][0] && tablero[1][1] == tablero[2][2]) && tablero[1][1] != ' ') {
            return 1;
        }
        /* Chequea diagonal secundaria. */
        if ( ((row==0 && col==2) || (row==1 && col==1) || (row==2 && col==0)) && (tablero[1][1] == tablero[0][2] && tablero[1][1] == tablero[2][0]) && tablero[1][1] != ' ') { 
            return 1;
        }
    }
    /* No winner, yet. */
    return 0;
}

  
int main() 
{ 
    int sockfd, connfd; 
    struct sockaddr_in servaddr, cli; 
  
    // socket create and varification 
    sockfd = socket(AF_INET, SOCK_STREAM, 0); 
    if (sockfd == -1) { 
        printf("socket creation failed...\n"); 
        exit(0); 
    } 
    else
        printf("Socket successfully created..\n"); 
    bzero(&servaddr, sizeof(servaddr)); 
  
    // assign IP, PORT 
    servaddr.sin_family = AF_INET; 
    servaddr.sin_addr.s_addr = inet_addr("127.0.0.1"); 
    servaddr.sin_port = htons(PORT); 
  
    // connect the client socket to server socket 
    if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0) { 
        printf("connection with the server failed...\n"); 
        exit(0); 
    } 
    else
        printf("connected to the server..\n"); 
  
    // function for chat 
    func(sockfd); 
  
    // close the socket 
    close(sockfd); 
} 
